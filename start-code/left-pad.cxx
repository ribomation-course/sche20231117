#include <string>

namespace ribomation::text {
    auto leftPad(std::string txt, unsigned size, char pad = '*') -> std::string {
        if (size < txt.size()) return txt.substr(0, size);
        if (size == txt.size()) return txt;
        return txt + std::string(size - txt.size(), pad);
    }
}
