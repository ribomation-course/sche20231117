#include <iostream>
#include "gtest/gtest.h"

auto fibonacci(unsigned n) -> unsigned long {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

TEST(FIBONACCI, basic) {
    EXPECT_EQ(fibonacci(0), 0UL);
    EXPECT_EQ(fibonacci(1), 1UL);
}

TEST(FIBONACCI, typical) {
    EXPECT_EQ(fibonacci(10), 55UL);
}

