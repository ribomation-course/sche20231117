#pragma once
#include <string>

namespace ribomation::text {
    auto leftPad(std::string txt, unsigned size, char pad = '*') -> std::string;
}
