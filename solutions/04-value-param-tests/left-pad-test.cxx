#include <tuple>
#include "gtest/gtest.h"
#include "left-pad.hxx"

using namespace std::string_literals;
using std::tuple;
using std::string;
using testing::TestWithParam;
using testing::Values;


using ArgRes = tuple<string, unsigned, char, string>;
struct ArgResSuite : TestWithParam<ArgRes> {};

TEST_P(ArgResSuite, leftPadTst) {
    using ribomation::text::leftPad;
    auto [txt, siz, pad, res] = GetParam();
    EXPECT_EQ(leftPad(txt, siz, pad), res);
}

INSTANTIATE_TEST_SUITE_P(leftPadParams, ArgResSuite, Values(
        tuple{"abc"s,    6, '*', "abc***"s},
        tuple{"abc"s,    6, '-', "abc---"s},
        tuple{"abc"s,    3, '-', "abc"s},
        tuple{"abcdef"s, 3, '-', "abc"s},
        tuple{"abcdef"s, 0, '-', ""s},
        tuple{""s,       0, '*', ""s},
        tuple{""s,       4, '*', "****"s}
));
