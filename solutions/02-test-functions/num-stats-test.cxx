#include "gtest/gtest.h"
#include "num-stats.hxx"


TEST(IntegralValues, justTen) {
    auto data = std::vector{10, 10, 10, 10};
    auto [avg, min, max] = ribomation::numbers::num_stats(data);
    EXPECT_EQ(avg, 10);
    EXPECT_EQ(min, 10);
    EXPECT_EQ(max, 10);
}

TEST(IntegralValues, oneToFive) {
    auto data = std::vector{1, 2, 3, 4, 5};
    auto [avg, min, max] = ribomation::numbers::num_stats(data);
    EXPECT_DOUBLE_EQ(avg, 3);
    EXPECT_EQ(min, 1);
    EXPECT_EQ(max, 5);
}

TEST(IntegralValues, empty) {
    auto data = std::vector<long>{};
    EXPECT_THROW(ribomation::numbers::num_stats(data), std::invalid_argument);
}

TEST(FloatValues, oneToFive) {
    auto data = std::vector<long double>{1, 2, 3, 4, 5};
    auto [avg, min, max] = ribomation::numbers::num_stats(data);
    EXPECT_DOUBLE_EQ(avg, 3.0);
    EXPECT_EQ(min, 1);
    EXPECT_EQ(max, 5);
}

TEST(FloatValues, pointFive) {
    auto data = std::vector{1.5, 2.0, 2.5};
    auto [avg, min, max] = ribomation::numbers::num_stats(data);
    EXPECT_DOUBLE_EQ(avg, 2);
    EXPECT_EQ(min, 1.5);
    EXPECT_EQ(max, 2.5);
}
