#pragma once
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace ribomation::numbers {

    template<typename ValueType>
    auto num_stats(std::vector<ValueType> const& data) -> std::tuple<double, ValueType, ValueType> {
        if (data.empty()) {
            throw std::invalid_argument{"no numbers/data"};
        }

        auto sum = std::accumulate(data.cbegin(), data.cend(), ValueType{});
        auto [minIt, maxIt] = std::minmax_element(data.cbegin(), data.cend());
        return {sum / data.size(), *minIt, *maxIt};
    }

}
