#include <fstream>
#include <filesystem>
namespace fs = std::filesystem;

#include "gtest/gtest.h"
#include "count.hxx"

namespace existing_file {
    struct ExistingFileTest : testing::Test {
        std::string const filename = "../silly-text.txt";
    protected:
        void SetUp() override {
            ASSERT_TRUE(fs::exists(fs::path{filename}));
        }
    };

    TEST_F(ExistingFileTest, count) {
        auto [L, W, C] = ribomation::text::count(filename);
        EXPECT_EQ(L, 7);
        EXPECT_EQ(W, 76);
        EXPECT_EQ(C, 421);
    }
}

namespace non_existing_file {
    struct NonExistingFileTest : testing::Test {
        std::string const filename = "../no-such-file.txt";
    };

    TEST_F(NonExistingFileTest, count) {
        EXPECT_THROW(ribomation::text::count(filename), std::invalid_argument);
    }
}

namespace generated_file {
    struct GeneratedFileTest : testing::Test {
        std::string const filename = "/tmp/generated-test-file.txt";

    protected:
        void SetUp() override {
            {
                auto file = std::ofstream{filename};
                file << "This is the first line\n";
                file << "This is the 2nd row\n";
                file << "Finally, here is the 3rd text line\n";
            }
            ASSERT_TRUE(fs::exists(fs::path{filename}));
        }

        void TearDown() override {
            fs::remove(fs::path{filename});
            ASSERT_FALSE(fs::exists(fs::path{filename}));
        }
    };

    TEST_F(GeneratedFileTest, count) {
        auto [L, W, C] = ribomation::text::count(filename);
        EXPECT_EQ(L, 3);
        EXPECT_EQ(W, 17);
        EXPECT_EQ(C, 75);
    }
}
