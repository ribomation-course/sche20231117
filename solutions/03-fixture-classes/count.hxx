#pragma once
#include <string>
#include <tuple>

namespace ribomation::text {

    auto count(std::string const& filename) -> std::tuple<unsigned, unsigned, unsigned>;

}
