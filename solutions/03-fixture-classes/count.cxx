#include <string>
#include <fstream>
#include <sstream>
#include <tuple>
#include <stdexcept>

namespace ribomation::text {

    auto count(std::string const& filename) -> std::tuple<unsigned, unsigned, unsigned> {
        auto file = std::ifstream{filename};
        if (!file) {
            throw std::invalid_argument{"cannot open file"};
        }

        auto lines = 0U, words = 0U, chars = 0U;
        for (std::string line; std::getline(file, line);) {
            ++lines;
            chars += line.size();
            auto buf = std::istringstream{line};
            for (std::string w; buf >> w;) ++words;
        }
        return {lines, words, chars};
    }

}
