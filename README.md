# Unit Testing in C++ with Google Test
## 2023 November

Welcome to this course that will get you up to speed with unit testing in Google Test (`gtest`).

# Links

* [Installation Instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/cxx/gtest/)

# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/gtest-course/my-solutions
    cd ~/gtest-course
    git clone <https url to this repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/gtest-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
