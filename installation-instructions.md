# Installation Instructions

In order to do the programming exercises of the course, you need to have a decent C++ compiler and an editor.
Although there are many ways to write/compile/run C++ applications, we suggest the following tools
for this course:

* Ubuntu Linux
* GNU C++
* CMake + make (or ninja)
* JetBrains CLion

## Disclaimer 🤔
**We don't _require_ you to install a specific environment. As long as you know how
to write, compile and run C++ applications, you are good to go with your environment
of choice.**


## Installation of Linux and a C++ Compiler

Follow the instructions below:

* [Linux and C++](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)

## Installation of CLion

Our favourite IDE for C++ development is CLion. It has very nice built-in support for running `gtest` tests.
For this course, install the 30-days trial version.

* [JetBrains CLion IDE](https://www.jetbrains.com/clion/)

## Installation of Google Test

`gtest` is an external library that need to be downloaded and compiled.
You don't need to install `gtest` before the course, as we will go through the various 
installation options in the beginning of the course and settle for letting CMake do 
the download and compilation.
You can find the `gtest` source code repo at GitHub

* [Google Test Repo](https://github.com/google/googletest)

